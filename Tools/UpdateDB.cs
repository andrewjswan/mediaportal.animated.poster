// ttNNNNNNN_poster_0_Clean_original.gif
// ttNNNNNNN_poster_0_Lang_original.gif

using System; 
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Drawing;    
using System.Drawing.Imaging;
using System.Collections.Generic; 
using System.Text.RegularExpressions;
using Newtonsoft.Json; 

public class UpdateDB
{ 

    public static string[] GetFilesFromFolder (string path, string mask = "*.*", bool all = false) 
    { 
      if (!System.IO.Directory.Exists(path))
      {
        return null;
      }
      return System.IO.Directory.GetFiles(path, mask, (all ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)); 
    }

    // TMDB

    public class MovieDatails
    {
      public bool adult { get; set; }
      public string backdrop_path { get; set; }
      public Collections belongs_to_collection { get; set; }
      public int budget { get; set; }
      public List<Genre> genres { get; set; }
      public string homepage { get; set; }
      public int id { get; set; }
      public string imdb_id { get; set; }
      public string original_language { get; set; }
      public string original_title { get; set; }
      public string overview { get; set; }
      public double popularity { get; set; }
      public string poster_path { get; set; }
      public List<ProductionCompany> production_companies { get; set; }
      public List<ProductionCountry> production_countries { get; set; }
      public string release_date { get; set; }
      public string revenue { get; set; }
      public int runtime { get; set; }
      public List<SpokenLanguage> spoken_languages { get; set; }
      public string status { get; set; }
      public string tagline { get; set; }
      public string title { get; set; }
      public bool video { get; set; }
      public double vote_average { get; set; }
      public int vote_count { get; set; }
    }

    public class Collections
    {
      public int id { get; set; }
      public string name { get; set; }
      public string overview { get; set; }
      public string poster_path { get; set; }
      public string backdrop_path { get; set; }
    }
    public class Genre
    {
      public int id { get; set; }
      public string name { get; set; }
    }

    public class ProductionCompany
    {
      public string name { get; set; }
      public int id { get; set; }
    }

    public class ProductionCountry
    {
      public string iso_3166_1 { get; set; }
      public string name { get; set; }
    }

    public class SpokenLanguage
    {
      public string iso_639_1 { get; set; }
      public string name { get; set; }
    }

    public struct TMDBMovie
    {
      /// <summary>
      ///  TITLE
      /// </summary>
      public string Title;

      /// <summary>
      ///  ORIGINAL TITLE
      /// </summary>
      public string OriginalTitle;

      public int ID;

      /// <summary>
      /// IMDB Number
      /// </summary>
      public string IMDBNumber;

      /// <summary>
      /// Production Year
      /// </summary>
      public int Year;

    } // EOC

    public const string API_KEY = "e224fe4f3fec5f7b5570641f7cd3df3a"; // Inset API Key
    public static string API_LANG = "en";

    private const string API_BaseUrl = "http://api.themoviedb.org/3/";
    public const string API_MovieInfo = API_BaseUrl + "movie/{0}";
    public const string API_CollectionInfo = API_BaseUrl + "collection/{0}";

    private static string GetTMDBMovieDetailsURL(string url)
    {
      return MakeTMDBUrl(API_MovieInfo, url);
    }

    private static string GetTMDBCollectionDetailsURL(string url)
    {
      return MakeTMDBUrl(API_CollectionInfo, url);
    }

    private static string MakeTMDBUrl(string url, string what, string query = "", int year = 0)
    {
      string sURL = url.Replace("{0}", what) + "?api_key=" + API_KEY;
      if (!string.IsNullOrEmpty(API_LANG))
      {
        sURL += "&language=" + API_LANG;
      }
      if (!string.IsNullOrEmpty(query))
      {
        sURL += "&query='" + query + "'";
      }
      if (year > 0)
      {
        sURL += "&year" + year;
      }
      return sURL;
    }

    public static TMDBMovie GetTMDBDetails(string url, bool collection = false)
    {
      TMDBMovie oReturn = new TMDBMovie();
      string json = string.Empty;
      if (string.IsNullOrEmpty(url))
      {
        return oReturn;
      }

      try
      {
        string sRequest = collection ? GetTMDBCollectionDetailsURL(url) : GetTMDBMovieDetailsURL(url);
        json = GetWebServerResponse(sRequest);
      }
      catch
      {
        return oReturn;
      }
      if (string.IsNullOrEmpty(json))
      {
        return oReturn;
      }

      try
      {
        if (!collection)
        {
          MovieDatails Movie = JsonConvert.DeserializeObject<MovieDatails>(json);
          if (Movie == null || Movie.id <= 0)
          {
            return oReturn;
          }

          oReturn.ID = Movie.id;
          oReturn.Title = Movie.title;
          oReturn.OriginalTitle = Movie.original_title;
          oReturn.IMDBNumber = Movie.imdb_id;
          string sYear = !string.IsNullOrWhiteSpace(Movie.release_date) && Movie.release_date.Length >= 4 ? Movie.release_date.Substring(0, 4) : string.Empty;
          if (!string.IsNullOrEmpty(sYear))
          {
            int iYear;
            int.TryParse(sYear, out iYear);
            oReturn.Year = iYear;
          }
        }
        else
        {
          Collections Collection = JsonConvert.DeserializeObject<Collections>(json);
          if (Collection == null || Collection.id <= 0)
          {
            return oReturn;
          }
          oReturn.ID = Collection.id;
          oReturn.Title = Collection.name;
        }
      }
      catch { }

      return oReturn;
    }

    // Animated

    public class Entry 
    { 
      public string id { get; set; } 
      public string source { get; set; } 
      public string image { get; set; } 
      public string type { get; set; } 
      public string dateAdded { get; set; } 
      public string contributedBy { get; set; } 
      public string language { get; set; } 
      public string size { get; set; } 
      public string height { get; set; } 
      public string width { get; set; } 
      public string frames { get; set; } 
    } 

    public class Movie 
    { 
      public string imdbid { get; set; } 
      public string tmdbid { get; set; } 
      public string title { get; set; }
      public string year { get; set; } 
      public List<Entry> entries { get; set; } 
    }

    public class Collection
    { 
      public string tmdbid { get; set; } 
      public string title { get; set; }
      public List<Entry> entries { get; set; } 
    }

    public class RootObject 
    { 
      public List<Movie> movies { get; set; } 
      public List<Collection> collections { get; set; } 
      public int version { get; set; } 
      public string lastUpdated { get; set; } 
      public string previousUpdated { get; set; }
      public string baseURLPoster { get; set; }
      public string baseURLBackground { get; set; }
    }

    private static RootObject Catalog = null;
    private const string CatalogFilename = @"..\DB\movies.json";

    public static void LoadCatalog()
    {
      try
      {
        if (File.Exists(CatalogFilename))
        {
          Catalog = JsonConvert.DeserializeObject<RootObject>(File.ReadAllText(CatalogFilename));
          if (Catalog != null)
          {
            Console.WriteLine("Animated: Catalog: Version: {0}, Updated: {1}", Catalog.version, Catalog.lastUpdated);
          }
          else
          {
            Console.WriteLine("Animated: Catalog: Not loaded, DB changed its format? ... Or corrupted...");
          }
        }
      }
      catch (WebException we)
      {
        Console.WriteLine("! Animated: LoadCatalog: " + we);
      }
    }

    #region Web

    private static string GetWebServerResponse(string srequest)
    {
      string sReturn = string.Empty;
      System.Net.HttpWebRequest oHttpWebRequest;
      try
      {
        oHttpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(srequest);
        oHttpWebRequest.Timeout = 5000;
        oHttpWebRequest.Method = System.Net.WebRequestMethods.Http.Get;
        oHttpWebRequest.Accept = "application/json";
        System.Net.HttpWebResponse oResponse = (System.Net.HttpWebResponse)oHttpWebRequest.GetResponse();

        using (System.IO.StreamReader sr = new System.IO.StreamReader(oResponse.GetResponseStream()))
        {
          sReturn = sr.ReadToEnd();
        }
        oResponse.Close();
      }
      catch { }
      finally
      {
        oHttpWebRequest = null;
      }
      return sReturn;
    }

    #endregion Web

  // Main
  static bool updateFlag = false;

  public static void UpdateDBbyType(string type)
  {
    string imageFolder = @"..\..\mediaportal.animated." + type + @"\" + type;
    Console.WriteLine("# Find new " + type + "s ... in " + imageFolder);
    string[] posterFiles = GetFilesFromFolder(imageFolder, "tt*original.gif");
    if (posterFiles != null && posterFiles.Count() > 0)
    {
      List<string> lstMovies = new List<string>();
      foreach (string poster in posterFiles)
      {
        string fileName = Path.GetFileName(poster);
        //
        string imdbMovieID = string.Empty;
        Regex rximdbid = new Regex(@"(?<imdbid>tt\d{7,9})");
        Match matchimdbid = rximdbid.Match(fileName);
        if (matchimdbid.Success)
        {
          // Get IMDBID
          imdbMovieID = matchimdbid.Groups["imdbid"].Value.ToLowerInvariant();
          // Console.WriteLine("- " + fileName + " IMDB: " + imdbMovieID);
        }
        if (!string.IsNullOrEmpty(imdbMovieID))
        {
          if (!lstMovies.Contains(imdbMovieID))
          {
            lstMovies.Add(imdbMovieID);
          }
        }
      }
      //
      if (lstMovies != null && lstMovies.Count > 0)
      {
        foreach (string imdb in lstMovies)
        {
          Movie dbMovieLine = Catalog.movies.Find(x => x.imdbid == imdb); 
          TMDBMovie movie = new TMDBMovie();
          if (dbMovieLine == null || (dbMovieLine != null && (string.IsNullOrEmpty(dbMovieLine.title) || string.IsNullOrEmpty(dbMovieLine.year))))
          {
            Console.WriteLine("- Get details from TMDB: " + imdb);
            movie = GetTMDBDetails(imdb);
          }
          else
          {
            Console.WriteLine("- Get details from DB: " + imdb);
            movie.IMDBNumber = dbMovieLine.imdbid;
            int.TryParse(dbMovieLine.tmdbid, out movie.ID);
            movie.Title = dbMovieLine.title;
            int.TryParse(dbMovieLine.year, out movie.Year);
          }
          if (string.IsNullOrEmpty(movie.IMDBNumber))
          {
            Console.WriteLine("!  Details for " + imdb + " - not found ...");
            continue;
          }

          string[] imdbFiles = GetFilesFromFolder(imageFolder, imdb + "*original.gif");
          if (imdbFiles != null && imdbFiles.Count() > 0)
          {
            bool newMovie = false;
            Movie movieLine = Catalog.movies.Find(x => x.imdbid == imdb); 
            if (movieLine == null)
            {
              Console.WriteLine("New: " + imdb + " - " + movie.ID + " - " + movie.IMDBNumber + " - " + movie.Title + " - " + movie.Year);
              movieLine = new Movie();
              movieLine.imdbid = movie.IMDBNumber; 
              movieLine.tmdbid = movie.ID.ToString();
              movieLine.title = movie.Title;
              movieLine.year = movie.Year.ToString();
              newMovie = true;
            }
            else
            {
              Console.WriteLine("     " + imdb + " - " + movie.ID + " - " + movie.IMDBNumber + " - " + movie.Title + " - " + movie.Year);
              if (string.IsNullOrEmpty(movieLine.tmdbid))
              {
                movieLine.tmdbid = movie.ID.ToString();
                updateFlag = true;
              }
              if (string.IsNullOrEmpty(movieLine.title))
              {
                movieLine.title = movie.Title;
                updateFlag = true;
              }
              if (string.IsNullOrEmpty(movieLine.year))
              {
                movieLine.year = movie.Year.ToString();
                updateFlag = true;
              }
            }
            if (movieLine.entries == null)
            {
              movieLine.entries = new List<Entry>();
            }

            foreach (string imdbFile in imdbFiles)
            {
              string fileName = Path.GetFileName(imdbFile).ToLowerInvariant();
              string fileFullName = Path.GetFullPath(imdbFile);
              // Console.WriteLine("- " + imdb + " - " + fileName + " - " + fileFullName);
              //
              long sizeInBytes = 0;
              try
              {
                FileInfo file = new FileInfo(fileFullName);
                sizeInBytes = file.Length;
                // Console.WriteLine("- " + imdb + " - " + fileName + " - " + sizeInBytes);
              }
              catch (Exception)
              {
                Console.WriteLine("! Failed loading file " + fileName);
                continue; 
              }
              //
              int frameCount = 0;
              int imageHeight = 0;
              int imageWidth = 0;
              try
              {
                Image gifImg = Image.FromFile(fileFullName);
                FrameDimension dimension = new FrameDimension(gifImg.FrameDimensionsList[0]);
                // Number of frames
                frameCount = gifImg.GetFrameCount(dimension);
                // Height
                imageHeight = gifImg.Height;
                // Width
                imageWidth = gifImg.Width;
                // Console.WriteLine("- " + imdb + " - " + fileName + " - h" + imageHeight + " x w" + imageWidth + " : " + frameCount);
                if (gifImg != null)
                {
                  gifImg.Dispose();
                }
              }
              catch (Exception)
              {
                Console.WriteLine("! Failed loading image " + fileName);
                continue; 
              }
             //
             string dbFileName = fileName;
             string transferIMDBID = string.Empty;
             string transferType = string.Empty;
             string transferNum = string.Empty;
             string transferLang = string.Empty;
             Regex rxTransfer = new Regex(@"(?<imdbid>tt\d{7,9})_(?<type>[^_]+?)_(?<num>\d+?)_((?<lang>[^_]+?)_)?original.gif");
             Match matchTransfer = rxTransfer.Match(fileName);
             if (matchTransfer.Success)
             {
               transferIMDBID = matchTransfer.Groups["imdbid"].Value;
               transferLang = matchTransfer.Groups["lang"].Value;
               transferNum = matchTransfer.Groups["num"].Value;
             }
             if (string.IsNullOrEmpty(transferLang))
             {
               transferLang = "En";
             }
             else
             {
               dbFileName = dbFileName.Replace("_" + transferLang,"");
               string newFileName = Path.GetFullPath(imageFolder + @"\" + dbFileName);
               if (File.Exists(newFileName))
               {
                 int num = 0;
                 do
                 {
                  num++;
                 }
                 while (num < 100 && File.Exists(newFileName.Replace("_" + transferNum + "_", "_" + num.ToString() + "_")));
                 newFileName = newFileName.Replace("_" + transferNum + "_", "_" + num.ToString() + "_");
                 dbFileName = dbFileName.Replace("_" + transferNum, "_" + num.ToString());
               }
               try
               {
                 System.IO.File.Move(fileFullName, newFileName);
               }
               catch
               {
                 Console.WriteLine("! Failed rename image " + fileFullName + " -> " + newFileName);
               }
               transferLang = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(transferLang.ToLowerInvariant());
             }
             dbFileName = dbFileName.Replace("_original","");

             Entry movieEntry = movieLine.entries.Find(x => x.image == dbFileName); 
             if (movieEntry == null)
             {
               movieEntry = new Entry();
               movieEntry.id = (movieLine.entries.Where(x => x.type == type).Select(x => int.Parse(x.id)).DefaultIfEmpty(-1).Max() + 1).ToString();
               movieEntry.source = "-";
               movieEntry.image = dbFileName;
               movieEntry.type = type;
               movieEntry.dateAdded = DateTime.Now.ToString("yyyy-MM-dd");
               movieEntry.contributedBy = "Team Mediaportal";
               movieEntry.language = transferLang;
               movieEntry.size = sizeInBytes.ToString();
               movieEntry.height = imageHeight.ToString();
               movieEntry.width = imageWidth.ToString();
               movieEntry.frames = frameCount.ToString();
               movieLine.entries.Add(movieEntry);
               Console.WriteLine("> Record added [" + type + "]: " + fileName);
               updateFlag = true;
             }
             else
             {
               if (string.IsNullOrEmpty(movieEntry.source))
               {
                 movieEntry.source = "-";
                 updateFlag = true;
               }
               if (string.IsNullOrEmpty(movieEntry.size) || movieEntry.size != sizeInBytes.ToString())
               {
                 movieEntry.size = sizeInBytes.ToString();
                 updateFlag = true;
               }
               if (string.IsNullOrEmpty(movieEntry.height) || movieEntry.height != imageHeight.ToString())
               {
                 movieEntry.height = imageHeight.ToString();
                 updateFlag = true;
               }
               if (string.IsNullOrEmpty(movieEntry.width) || movieEntry.width != imageWidth.ToString())
               {
                 movieEntry.width = imageWidth.ToString();
                 updateFlag = true;
               }
               if (string.IsNullOrEmpty(movieEntry.frames) || movieEntry.frames != frameCount.ToString())
               {
                 movieEntry.frames = frameCount.ToString();
                 updateFlag = true;
               }
             }

            }

            if (newMovie)
            {
              Catalog.movies.Add(movieLine);
            }

            // Remove deleted files from DB
            string[] dbFileList = movieLine.entries.Where(x => x.type == type).Select(x => x.image).ToArray(); 
            foreach (string fileinlist in dbFileList)
            {
              string folderFileName = fileinlist.Replace(".gif", "_original.gif");
              if (!File.Exists(imageFolder + @"\" + folderFileName))
              {
                movieLine.entries.RemoveAll(x => x.type == type && x.image == fileinlist);
                Console.WriteLine("> Record removed [" + type + "]: " + folderFileName);
                updateFlag = true;
              }
            }
          }
        }
      }
    }
  }

  public static void UpdateDBCollectionbyType(string type)
  {
    string imageFolder = @"..\..\mediaportal.animated." + type + @"\Collection";
    Console.WriteLine("# Find new " + type + "s ... in " + imageFolder);
    string[] posterFiles = GetFilesFromFolder(imageFolder, "tmdb*original.gif");
    if (posterFiles != null && posterFiles.Count() > 0)
    {
      List<string> lstCollections = new List<string>();
      foreach (string poster in posterFiles)
      {
        string fileName = Path.GetFileName(poster);
        //
        string tmdbidCollectionID = string.Empty;
        Regex rxtmdbid = new Regex(@"tmdb_(?<tmdbid>\d+?)_");
        Match matchtmdbid = rxtmdbid.Match(fileName);
        if (matchtmdbid.Success)
        {
          // Get TMDBID
          tmdbidCollectionID = matchtmdbid.Groups["tmdbid"].Value.ToLowerInvariant();
          // Console.WriteLine("- " + fileName + " TMDB: " + tmdbidCollectionID);
        }
        if (!string.IsNullOrEmpty(tmdbidCollectionID))
        {
          if (!lstCollections.Contains(tmdbidCollectionID))
          {
            lstCollections.Add(tmdbidCollectionID);
          }
        }
      }
      //
      if (lstCollections != null && lstCollections.Count > 0)
      {
        foreach (string tmdbid in lstCollections)
        {
          TMDBMovie collection = new TMDBMovie();
          Collection dbCollectionLine = Catalog.collections.Find(x => x.tmdbid == tmdbid); 
          if (dbCollectionLine == null || (dbCollectionLine != null && string.IsNullOrEmpty(dbCollectionLine.title)))
          {
            Console.WriteLine("- Get details from TMDB: " + tmdbid);
            collection = GetTMDBDetails(tmdbid, true);
          }
          else
          {
            Console.WriteLine("- Get details from DB: " + tmdbid);
            int.TryParse(dbCollectionLine.tmdbid, out collection.ID);
            collection.Title = dbCollectionLine.title;
          }
          if (collection.ID <= 0)
          {
            Console.WriteLine("!  Details for " + tmdbid + " - not found ...");
            continue;
          }

          string[] tmdbidFiles = GetFilesFromFolder(imageFolder, "tmdb_" + tmdbid + "_*original.gif");
          if (tmdbidFiles != null && tmdbidFiles.Count() > 0)
          {
            bool newCollection = false;
            Collection collectionLine = Catalog.collections.Find(x => x.tmdbid == tmdbid); 
            if (collectionLine == null)
            {
              Console.WriteLine("New: " + tmdbid + " - " + collection.ID + " - " + collection.Title);
              collectionLine = new Collection();
              collectionLine.tmdbid = collection.ID.ToString();
              collectionLine.title = collection.Title;
              newCollection = true;
            }
            else
            {
              Console.WriteLine("     " + tmdbid + " - " + collection.ID + " - " + collection.Title);
              if (string.IsNullOrEmpty(collectionLine.title))
              {
                collectionLine.title = collection.Title;
                updateFlag = true;
              }
            }
            if (collectionLine.entries == null)
            {
              collectionLine.entries = new List<Entry>();
            }

            foreach (string tmdbidFile in tmdbidFiles)
            {
              string fileName = Path.GetFileName(tmdbidFile).ToLowerInvariant();
              string fileFullName = Path.GetFullPath(tmdbidFile);
              // Console.WriteLine("- " + tmdbid + " - " + fileName + " - " + fileFullName);
              //
              long sizeInBytes = 0;
              try
              {
                FileInfo file = new FileInfo(fileFullName);
                sizeInBytes = file.Length;
                // Console.WriteLine("- " + tmdbid + " - " + fileName + " - " + sizeInBytes);
              }
              catch (Exception)
              {
                Console.WriteLine("! Failed loading file " + fileName);
                continue; 
              }
              //
              int frameCount = 0;
              int imageHeight = 0;
              int imageWidth = 0;
              try
              {
                Image gifImg = Image.FromFile(fileFullName);
                FrameDimension dimension = new FrameDimension(gifImg.FrameDimensionsList[0]);
                // Number of frames
                frameCount = gifImg.GetFrameCount(dimension);
                // Height
                imageHeight = gifImg.Height;
                // Width
                imageWidth = gifImg.Width;
                // Console.WriteLine("- " + tmdbid + " - " + fileName + " - h" + imageHeight + " x w" + imageWidth + " : " + frameCount);
                if (gifImg != null)
                {
                  gifImg.Dispose();
                }
              }
              catch (Exception)
              {
                Console.WriteLine("! Failed loading image " + fileName);
                continue; 
              }
             //
             string dbFileName = fileName;
             string transferTMDBID = string.Empty;
             string transferType = string.Empty;
             string transferNum = string.Empty;
             string transferLang = string.Empty;
             Regex rxTransfer = new Regex(@"tmdb_(?<tmdbid>\d+?)_(?<type>[^_]+?)_(?<num>\d+?)_((?<lang>[^_]+?)_)?original.gif");
             Match matchTransfer = rxTransfer.Match(fileName);
             if (matchTransfer.Success)
             {
               transferTMDBID = matchTransfer.Groups["tmdbid"].Value;
               transferLang = matchTransfer.Groups["lang"].Value;
               transferNum = matchTransfer.Groups["num"].Value;
             }
             if (string.IsNullOrEmpty(transferLang))
             {
               transferLang = "En";
             }
             else
             {
               dbFileName = dbFileName.Replace("_" + transferLang,"");
               string newFileName = Path.GetFullPath(imageFolder + @"\" + dbFileName);
               if (File.Exists(newFileName))
               {
                 int num = 0;
                 do
                 {
                  num++;
                 }
                 while (num < 100 && File.Exists(newFileName.Replace("_" + transferNum + "_", "_" + num.ToString() + "_")));
                 newFileName = newFileName.Replace("_" + transferNum + "_", "_" + num.ToString() + "_");
                 dbFileName = dbFileName.Replace("_" + transferNum, "_" + num.ToString());
               }
               try
               {
                 System.IO.File.Move(fileFullName, newFileName);
               }
               catch
               {
                 Console.WriteLine("! Failed rename image " + fileFullName + " -> " + newFileName);
               }
               transferLang = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(transferLang.ToLowerInvariant());
             }
             dbFileName = dbFileName.Replace("_original","");

             Entry collectionEntry = collectionLine.entries.Find(x => x.image == dbFileName); 
             if (collectionEntry == null)
             {
               collectionEntry = new Entry();
               collectionEntry.id = (collectionLine.entries.Where(x => x.type == type).Select(x => int.Parse(x.id)).DefaultIfEmpty(-1).Max() + 1).ToString();
               collectionEntry.source = "-";
               collectionEntry.image = dbFileName;
               collectionEntry.type = type;
               collectionEntry.dateAdded = DateTime.Now.ToString("yyyy-MM-dd");
               collectionEntry.contributedBy = "Team Mediaportal";
               collectionEntry.language = transferLang;
               collectionEntry.size = sizeInBytes.ToString();
               collectionEntry.height = imageHeight.ToString();
               collectionEntry.width = imageWidth.ToString();
               collectionEntry.frames = frameCount.ToString();
               collectionLine.entries.Add(collectionEntry);
               Console.WriteLine("> Record added [" + type + "]: " + fileName);
               updateFlag = true;
             }
             else
             {
               if (string.IsNullOrEmpty(collectionEntry.source))
               {
                 collectionEntry.source = "-";
                 updateFlag = true;
               }
               if (string.IsNullOrEmpty(collectionEntry.size) || collectionEntry.size != sizeInBytes.ToString())
               {
                 collectionEntry.size = sizeInBytes.ToString();
                 updateFlag = true;
               }
               if (string.IsNullOrEmpty(collectionEntry.height) || collectionEntry.height != imageHeight.ToString())
               {
                 collectionEntry.height = imageHeight.ToString();
                 updateFlag = true;
               }
               if (string.IsNullOrEmpty(collectionEntry.width) || collectionEntry.width != imageWidth.ToString())
               {
                 collectionEntry.width = imageWidth.ToString();
                 updateFlag = true;
               }
               if (string.IsNullOrEmpty(collectionEntry.frames) || collectionEntry.frames != frameCount.ToString())
               {
                 collectionEntry.frames = frameCount.ToString();
                 updateFlag = true;
               }
             }

            }

            if (newCollection)
            {
              Catalog.collections.Add(collectionLine);
            }

            // Remove deleted files from DB
            string[] dbFileList = collectionLine.entries.Where(x => x.type == type).Select(x => x.image).ToArray(); 
            foreach (string fileinlist in dbFileList)
            {
              string folderFileName = fileinlist.Replace(".gif", "_original.gif");
              if (!File.Exists(imageFolder + @"\" + folderFileName))
              {
                collectionLine.entries.RemoveAll(x => x.type == type && x.image == fileinlist);
                Console.WriteLine("> Record removed [" + type + "]: " + folderFileName);
                updateFlag = true;
              }
            }
          }
        }
      }
    }
  }

  public static void Main() 
  {
    LoadCatalog();
    if (Catalog == null)
    {
      Catalog = new RootObject();
      Catalog.version = 0;
      Catalog.lastUpdated = DateTime.Now.ToString("yyyy-MM-dd");
      Catalog.previousUpdated = DateTime.Now.ToString("yyyy-MM-dd");
      Catalog.baseURLPoster = string.Empty;
      Catalog.baseURLBackground = string.Empty;
      Catalog.movies = new List<Movie>();
      Catalog.collections = new List<Collection>();
    }
    else
    {
      if (Catalog.movies == null)
      {
        Catalog.movies = new List<Movie>();
      }
      if (Catalog.collections == null)
      {
        Catalog.collections = new List<Collection>();
      }
    }

    UpdateDBbyType("poster");
    UpdateDBbyType("background");
    UpdateDBCollectionbyType("poster");
    UpdateDBCollectionbyType("background");

    if (updateFlag)
    {
      Catalog.version++;
      Catalog.previousUpdated = Catalog.lastUpdated;
      Catalog.lastUpdated = DateTime.Now.ToString("yyyy-MM-dd");
      Catalog.baseURLPoster = "https://gitlab.com/andrewjswan/mediaportal.animated.poster";
      Catalog.baseURLBackground = "https://gitlab.com/andrewjswan/mediaportal.animated.background";
      Console.WriteLine("# Catalog updated: " + CatalogFilename);
    }
    if (Catalog.movies.Count > 0 || Catalog.collections.Count > 0)
    {
      string resCatalog = JsonConvert.SerializeObject(Catalog);
      File.WriteAllText(CatalogFilename, resCatalog);
      Console.WriteLine("# Catalog saved: " + CatalogFilename);
    }
  }
}
